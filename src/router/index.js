import Vue from 'vue'
import VueRouter from 'vue-router'
import Welcome from '../views/pages/Welcome.vue'
import auth from '../utils/auth'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('../views/layouts/Default.vue'),
    children: [{
      path: '/',
      name: 'Home',
      component: () => import('../views/pages/Home.vue')
    },
    {
      path: '/public',
      name: 'Public',
      component: () => import('../views/pages/Public.vue')
    }],
    beforeEnter: auth
  },
  {
    path: '/welcome',
    name: 'Welcome',
    component: Welcome,

  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})



export default router
