import Vue from 'vue'
import Vuex from 'vuex'
import { Axios } from '../utils/axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentUser: [],
},
mutations: {
    setUser(state, user) {
        state.currentUser = user
    }
},
actions: {
    updateUser({
        commit
    }) {
        Axios.get('details')
            .then(response => {
                commit('setUser', response.data.success)
                console.log(response.data)
            })
            .catch(error =>
              console.log(error.response)
              )
          }
        }
    })
    