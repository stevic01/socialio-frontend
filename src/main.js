import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {BootstrapVue, } from 'bootstrap-vue/dist/bootstrap-vue.esm'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { Axios } from './utils/axios'


Vue.config.productionTip = false

Vue.use(BootstrapVue);

// Inject Axios
Vue.prototype.$axios = Axios


// Set Vue authentication
// axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api` 

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
