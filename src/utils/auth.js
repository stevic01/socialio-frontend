
const token = localStorage.getItem('ACCESS_TOKEN') || ''

export default (to, from, next) => {
    if (token != null && token.length > 0) {
        next()
    } else {
        localStorage.removeItem('ACCESS_TOKEN')
        next('/welcome')
    }
}