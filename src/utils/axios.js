import axios from 'axios'
import Router from '../router'

// Here you create your own instance of axios, and configs

const Axios = axios.create({
    baseURL: process.env.NODE_ENV === 'development'
    ? 'http://localhost:8000/api/'
    : '',
    withCredentials: false // ovo ne znam da li treba za laravel, ima veze sa cors, nek stoji nece smeteti
})


// Add a request interceptor
// Znaci injectujemo svoj kod u svaki request koji axios salje
// Trazimo token u local strage, ako ga ima injectujemo ga u header, da bi app radila, posto nemamo sesiju kao 
// 
Axios.interceptors.request.use(
    config => {
        const token = localStorage.getItem('ACCESS_TOKEN')
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        // config.headers['Content-Type'] = 'application/json';
        return config;
    },
    error => {
        Promise.reject(error)
    });

Axios.interceptors.response.use((response) => {
    return response
    }, error => {
    if (error.response.status === 401) {
        localStorage.removeItem('ACCESS_TOKEN')
        Router.push("/welcome", () => {});
    }
    return Promise.reject(error)
    })
    

export { Axios }